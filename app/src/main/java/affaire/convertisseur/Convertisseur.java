package affaire.convertisseur;

public class Convertisseur {

    private float poidsKg, poidsLbs;

    public Convertisseur(float valeur) throws Exception {
        if(valeur < 0f || valeur > 1000000.0f)
            throw new Exception("La valeur doit être comprise entre 0.0 et 1000000.0");
        poidsLbs = valeur * 2.20462f;
        poidsKg = valeur * 0.453592f;
    }

    public float getPoidsKg() {
        return poidsKg;
    }

    public float getPoidsLbs() {
        return poidsLbs;
    }
}
