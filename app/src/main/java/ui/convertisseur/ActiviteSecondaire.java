package ui.convertisseur;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import affaire.convertisseur.Convertisseur;

public class ActiviteSecondaire extends AppCompatActivity {

    float valeur;
    Convertisseur convertisseur;
    TextView tvPoidsLbs, tvPoidsKg;
    Button btnFinAffichage;
    Context cx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cx = this.getApplicationContext();
        initialiserVue();
        recupererAfficher();
    }

    private void initialiserVue(){
        setContentView(R.layout.activity_activite_secondaire);
        tvPoidsKg = this.findViewById(R.id.tvPoidsKg);
        tvPoidsLbs = this.findViewById(R.id.tvPoidsLbs);
        btnFinAffichage = this.findViewById(R.id.btnFinAffichage);

        btnFinAffichage.setOnClickListener(view -> {
            Intent intention = new Intent(this, ActivitePrincipale.class);
            startActivity(intention);
        });
    }

    private void recupererAfficher(){
        valeur = this.getIntent().getFloatExtra("valeur", 0f);
        try {
            convertisseur = new Convertisseur(valeur);
            tvPoidsLbs.setText("" + convertisseur.getPoidsLbs());
            tvPoidsKg.setText("" + convertisseur.getPoidsKg());
        } catch (Exception e) {
            Toast.makeText(cx, e.getMessage(), Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }
}