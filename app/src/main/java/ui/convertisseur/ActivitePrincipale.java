package ui.convertisseur;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivitePrincipale extends AppCompatActivity {

    EditText etPoids;
    Button btnOk;
    Context cx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cx = this;
        initialiserVue();
    }

    private void initialiserVue() {
        setContentView(R.layout.activity_activite_principale);

        etPoids = this.findViewById(R.id.etPoids);
        btnOk = this.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(view -> {
            try {
                float valeurSaisie = Float.parseFloat(etPoids.getText().toString());
                Intent intent = new Intent(cx, ActiviteSecondaire.class);
                intent.putExtra("valeur", valeurSaisie);
                this.startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(
                        cx,
                        e.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
            }

        });
    }
}